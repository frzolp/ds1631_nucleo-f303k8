/*
 * ds1631.h
 *
 *  Created on: Mar 27, 2017
 *      Author: Francis
 */

#ifndef DS1631_H_
#define DS1631_H_

#include "i2c.h"

typedef struct config_reg {
	uint8_t oneshot : 1;// 0 = continuous conversion, 1 = single conversion
	uint8_t pol : 1;	// 0 = Tout active low, 1 = Tout active high
	uint8_t r0 : 1;		// Resolution bit 0
	uint8_t r1 : 1;		// Resolution bit 1
	uint8_t nvb : 1;	// 0 = NVRAM not busy, 1 = write in progress
	uint8_t tlf : 1;	// 0 = low temp not exceeded, 1 = exceeded
	uint8_t thf : 1;	// 0 = high temp not exceeded, 1 = exceeded
	uint8_t done : 1;	// 0 = conversion in progress, 1 = complete
} config_reg;

void ds1631_get_config(I2C_HandleTypeDef* handle, uint8_t addr, config_reg* config);
void ds1631_set_config(I2C_HandleTypeDef* handle, uint8_t addr, config_reg* config);
void ds1631_start_convert(I2C_HandleTypeDef* handle, uint8_t addr);
void ds1631_stop_convert(I2C_HandleTypeDef* handle, uint8_t addr);
float ds1631_read_temp_f(I2C_HandleTypeDef* handle, uint8_t addr, config_reg* config);
float ds1631_read_temp_c(I2C_HandleTypeDef* handle, uint8_t addr, config_reg* config);

#endif /* DS1631_H_ */
