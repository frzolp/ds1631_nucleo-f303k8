/*
 * ds1631.c
 *
 *  Created on: Mar 27, 2017
 *      Author: Francis
 */

#include "ds1631.h"

#define START_CONVERT	0x51
#define STOP_CONVERT	0x22
#define READ_TEMP		0xAA
#define ACCESS_TH		0xA1
#define ACCESS_TL		0xA2
#define ACCESS_CONFIG	0xAC
#define SOFT_RESET		0x54

extern void Error_Handler();

void send_read_request(I2C_HandleTypeDef* handle, uint8_t addr, uint8_t reg, uint8_t* buf, uint8_t len);
void send_write_request(I2C_HandleTypeDef* handle, uint8_t addr, uint8_t reg, uint8_t* buf, uint8_t len);

/**
 * @brief Requests the contents of the config register
 * @param handle I2C peripheral handle
 * @param addr I2C slave address
 * @param config Pointer to config_reg bitfield to store data
 */
void ds1631_get_config(I2C_HandleTypeDef* handle, uint8_t addr, config_reg* config) {
	send_read_request(handle, addr, ACCESS_CONFIG, (uint8_t*)config, sizeof(config_reg));
}

/**
 * @brief Sets the contents of the config register
 * @param handle I2C peripheral handle
 * @param addr I2C slave address
 * @param config Pointer to config_reg bitfield to store data
 */
void ds1631_set_config(I2C_HandleTypeDef* handle, uint8_t addr, config_reg* config) {
	send_write_request(handle, addr, ACCESS_CONFIG, (uint8_t*)config, sizeof(config_reg));
}

/**
 * @brief Begins a temperature conversion operation
 * @param handle I2C peripheral handle
 * @param addr I2C slave address
 */
void ds1631_start_convert(I2C_HandleTypeDef* handle, uint8_t addr) {
	send_write_request(handle, addr, START_CONVERT, NULL, 0);
}

/**
 * @brief Stops the temperature conversion operation
 * @param handle I2C peripheral handle
 * @param addr I2C slave address
 */
void ds1631_stop_convert(I2C_HandleTypeDef* handle, uint8_t addr) {
	send_write_request(handle, addr, STOP_CONVERT, NULL, 0);
}

/**
 * @brief Requests the temperature and converts to F
 * @param handle I2C peripheral handle
 * @param addr I2C slave address
 * @param config Configuration register value for temp resolution
 * @return Temperature reading in degrees fahrenheit
 */
float ds1631_read_temp_f(I2C_HandleTypeDef* handle, uint8_t addr, config_reg* config) {
	return ds1631_read_temp_c(handle, addr, config) * 1.8 + 32;
}

/**
 * @brief Requests the temperature and converts to C
 * @param handle I2C peripheral handle
 * @param addr I2C slave address
 * @param config Configuration register value for temp resolution
 * @return Temperature reading in degrees Celsius
 */
float ds1631_read_temp_c(I2C_HandleTypeDef* handle, uint8_t addr, config_reg* config) {
	uint8_t buf[2] = { 0 };

	send_read_request(handle, addr, READ_TEMP, buf, sizeof(buf) / sizeof(buf[0]));

	// Order and store the buffer's contents into a work integer
	int32_t tmp = ((int32_t)buf[0] << 8) | (int32_t)(buf[1]);
	// Has the sign bit been set? Adjust the 32-bit value to be negative
	if (tmp >= 0x8000) {
		tmp -= 0xFFFF;
	}

	/*
	 * In the DS1631 datasheet, the value of R0 and R1 fields in the
	 * configuration register determine the temperature resolution.
	 *
	 *  R1  R0  Resolution Steps
	 * ====================================
	 * | 0 | 0 | 9  bits  | 0.5 degrees   |
	 * | 0 | 1 | 10 bits  | 0.25 degrees  |
	 * | 1 | 0 | 11 bits  | 0.125 degrees |
	 * | 1 | 1 | 12 bits  | 0.0625 degrees|
	 *
	 * This also affects how much right shifting we need to do.
	 */
	if (config->r1) {
		if (config->r0) {			// R1=1,R0=1 - 12-bit resolution
			tmp >>= 4;
			return (tmp * 0.0625);
		} else {					// R1=1,R0=0 - 11-bit resolution
			tmp >>= 5;
			return (tmp * 0.125);
		}
	} else {
		if (config->r0) {			// R1=0,R0=1 - 10-bit resolution
			tmp >>= 6;
			return (tmp * 0.25);
		} else {					// R1=0,R0=0 - 9-bit resolution
			tmp >>= 7;
			return (tmp * 0.5);
		}
	}
}

/**
 * @brief Requests the contents of an I2C slave's register
 * @param handle I2C peripheral handle
 * @param addr I2C slave address
 * @param reg I2C slave data register address
 * @param buf Storage buffer
 * @param len Length of storage buffer
 */
void send_read_request(I2C_HandleTypeDef* handle, uint8_t addr, uint8_t reg, uint8_t* buf, uint8_t len) {
	// DS1631 addresses are 7 bits long. Left shift it by one,
	// setting the least significant bit to 1, indicating a read request
	uint16_t read_addr = (addr << 1) | 0x01;

	// Send the read command to the DS1631
	while (HAL_OK != HAL_I2C_Master_Transmit(handle, read_addr, &reg, sizeof(reg), 10000)) {
		// If something goes wrong, fall into the error handler.
		if (HAL_I2C_ERROR_AF != HAL_I2C_GetError(handle)) {
			Error_Handler();
		}
	}

	// Receive the data from the DS1631
	while (HAL_OK != HAL_I2C_Master_Receive(handle, read_addr, buf, len, 10000)) {
		// If something goes wrong, fall into the error handler.
		if (HAL_I2C_ERROR_AF != HAL_I2C_GetError(handle)) {
			Error_Handler();
		}
	}
}

/**
 * @brief Writes data to an I2C slave's register
 * @param handle I2C peripheral handle
 * @param addr I2C slave address
 * @param reg I2C slave data register address
 * @param buf Buffer to write
 * @param len Length of data to write
 */
void send_write_request(I2C_HandleTypeDef* handle, uint8_t addr, uint8_t reg, uint8_t* buf, uint8_t len) {
	// DS1631 addresses are 7 bits long. Left shift it by one,
	// leaving the least significant bit 0, indicating a write operation
	uint16_t write_addr = (addr << 1);

	// The DS1631 expects the data as soon as the write request is issued.
	// Make a bigger buffer, with the first byte containing the register address
	// and the remaining byte(s) holding the parameter data.
	uint8_t tx[len + 1];
	tx[0] = reg;
	for (uint8_t x = 1; x < (len + 1); x++) {
		tx[x] = *buf++;
	}

	// Transmit the register address and its contents to the slave
	while (HAL_OK != HAL_I2C_Master_Transmit(handle, write_addr, tx, len + 1, 10000)) {
		// If something goes wrong, fall into the error handler.
		if (HAL_I2C_ERROR_AF != HAL_I2C_GetError(handle)) {
			Error_Handler();
		}
	}
}
